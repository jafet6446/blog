<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome3');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tablero', 'TableroController@index')->name('tablero.index');

Route::get('/vehiculo/lista', 'VehiculoController@index')->name('vehiculo.index');
Route::get('/vehiculo/nuevo', 'VehiculoController@create')->name('vehiculo.create');
Route::post('/vehiculo/registrar', 'VehiculoController@store')->name('vehiculo.store');
Route::get('/vehiculo/editar/{id}', 'VehiculoController@edit')->name('vehiculo.edit');
Route::put('/vehiculo/editar/{id}', 'VehiculoController@update')->name('vehiculo.update');
Route::delete('/vehiculo/eliminar/{id}', 'VehiculoController@destroy')->name('vehiculo.delete');


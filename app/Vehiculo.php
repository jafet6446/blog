<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    protected $table='vehiculos';
	
	
	public function user()
   {
    return $this->belongsTo('User');
   }
}

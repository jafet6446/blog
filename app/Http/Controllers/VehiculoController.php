<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Vehiculo;
class VehiculoController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$usuario= Auth::user();
		$vehiculos = Vehiculo::all();
        return view('vehiculos.lista')->with(compact('vehiculos'))->with(compact('usuario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$usuario= Auth::user();
        return view('vehiculos.create')->with(compact('usuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vehiculo = new Vehiculo;
		$vehiculo->numeroSerie = $request->numeroserie;
		$vehiculo->marca = $request->marca;
		$vehiculo->modelo = $request->modelo;
		$vehiculo->anio = $request->anio;
		$vehiculo->clase = $request->clase;
		$vehiculo->gps = $request->gps;
		$vehiculo->descripcion = $request->descripcion;
		$vehiculo->usuarioId = $request->usuarioId;
		$vehiculo->save();
		$usuario= Auth::user();
		$vehiculos = Vehiculo::all();
        return redirect()->route('vehiculo.index');
		
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$usuario= Auth::user();
        $vehiculo = Vehiculo::find($id);
		return view('vehiculos.edit')->with(compact('vehiculo'))->with(compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vehiculo = Vehiculo::find($id);
		$vehiculo->numeroSerie = $request->numeroserie;
		$vehiculo->marca = $request->marca;
		$vehiculo->modelo = $request->modelo;
		$vehiculo->anio = $request->anio;
		$vehiculo->clase = $request->clase;
		$vehiculo->gps = $request->gps;
		$vehiculo->descripcion = $request->descripcion;
		$vehiculo->usuarioId = $request->usuarioId;
		$vehiculo->save();
		return redirect()->route('vehiculo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Vehiculo::find($id)->delete();
		return redirect()->route('vehiculo.index');
    }
}

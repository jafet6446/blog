<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Vehiculo;
class TableroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index()
    {
		$usuario= Auth::user();
		$vehiculos = Vehiculo::all();
        return view('tablero.Dash')->with(compact('vehiculos'))->with(compact('usuario'));
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatVehiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->bigIncrements('id');
            $table->string('numeroSerie')->unique();
			$table->String('marca');
			$table->string('modelo');
			$table->integer('anio');
			$table->string('clase');
			$table->bigInteger('gps')->unique();
			$table->text('descripcion');
			$table->biginteger('usuarioId')->unsigned();            
			$table->foreign('usuarioId')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos');
    }
}

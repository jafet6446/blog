<!DOCTYPE html>
@extends('layouts.app')

@section('content')
<html lang="es">
  <head>
    <title>Gps Tracking | Inicio</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      
      <link href="{{ asset('css/Home_Styles.css') }}" rel="stylesheet">
      
      <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
      
      <script src="https://kit.fontawesome.com/a02c47cedb.js" crossorigin="anonymous"></script>

   
  </head>
    
    <body> 
    <!-- Header -->
        
        <header id="header">
    <!-- Menú de Navegacion -->
        
    <!-- Final del Menú de Navegacion -->
    <!-- Banner -->    
			

            <div class="banner">
                <div class="banner_title">
                    <h1> Bienvenido a Gps Tracking System </h1>
                    <hr>
                    <p>La tecnología Gps al alcance de tu mano</p>
                </div>
            </div>
            <div class="Skew_down">
            </div>
    <!-- Final Banner -->        
        </header>
    <!-- Final del Header -->
    
    <!-- Acerca de nosotros -->
        <main>
            <section class="nosotros">
                <div class="info_nosotros">
                    <h1>Acerca de nosotros</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet consectetur adipiscing elit duis tristique sollicitudin nibh sit. Pulvinar proin gravida hendrerit lectus. Placerat duis ultricies lacus sed turpis. Aliquet enim tortor at auctor urna nunc id cursus metus. Venenatis lectus magna fringilla urna porttitor rhoncus dolor. Et tortor at risus viverra adipiscing at in tellus integer. Sit amet tellus cras adipiscing enim eu turpis egestas. Quis risus sed vulputate odio. Diam phasellus vestibulum lorem sed risus ultricies tristique nulla. Cras adipiscing enim eu turpis egestas pretium aenean pharetra. At consectetur lorem donec massa. Nullam ac tortor vitae purus faucibus ornare suspendisse sed. Egestas erat imperdiet sed euismod nisi porta lorem.</p>
                </div>
            
            </section>
        
        </main>
        
    <!-- Final Acerca de nosotros -->    
        
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
    
    </body>
</html>
@endsection
    
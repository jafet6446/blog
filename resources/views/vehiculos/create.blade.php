<!DOCTYPE html>
<html>
<head>
	<title>Vehiculos</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('CSS/auto_estilos.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a81368914c.js"></script>
	<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'/>
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="shape"></div>
	<div class="container">
		<div class="content">
            <h3>Ya tienes registrado un vehiculo?</h3>
			<p>
				Los datos regitrados a continuación son esenciales para poder 
				tener un mejor control del vehiculo que desea gestionar, esto 
				ayuda para saber que vehiculo esta localizando.
			</p>
            <button class="btn transparent" id="sign-up-btn">
              <a class="btn Ve" href="{{ route('vehiculo.index') }}">Mis Vehiculos</a> 
            </button>
		<div class="img">
			<img src="{{ asset('Img/gps.svg') }}">
		</div>
	</div>

		<div class="car-content">
			<form method="post" action="{{ route('vehiculo.store') }}">
			  @csrf
				<h2 class="title">Registra tu Automovil</h2>
           		<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Numero de serie" name="numeroserie" required >
           		   </div>
           		</div>
           		<div class="input-div pass">
           		   <div class="i"> 
					<i class='bx bxs-briefcase'></i>
           		   </div>
           		   <div class="div">
           		    	
           		    	<input type="text" placeholder="Marca" name="marca" required>
            	   </div>
            	</div>
				<div class="input-div pass">
					<div class="i"> 
						<i class='bx bxs-package'></i>
					</div>
					<div class="div">
						 
						 <input type="text" placeholder="Modelo"name= "modelo" required>
				 </div>
			  </div>
			  <div class="input-div pass">
				<div class="i"> 
					<i class='bx bx-calendar'></i>
				</div>
				<div class="div">
					 
					 <input type="text" placeholder="Año" name="anio" required>
			 </div>
		  </div>
		  <div class="input-div pass">
			<div class="i"> 
				<i class='bx bxs-car'></i>
			</div>
			<div class="div">
				 <select type="text" name="clase" id="clase" class="form-control" required>
					<option disabled selected>Clase</option>
					<option value="Automovil">Automovil</option>
					<option value="Camioneta">Camioneta</option>
					<option value="Camion">Camion</option>
					<option value="Motocicleta">Motocicleta</option>
					<option value="Bicicleta">Bicicleta</option>
					<option value="Trailer">Trailer</option>
			   </select>
		 </div>
	  </div>
			<div class="input-div one">
           		   <div class="i">
					<i class='bx bx-hash'></i>
           		   </div>
			  
           		   <div class="div">
           		   		
           		   		<input type="text" placeholder="Numero telefonico del dispisitivo GPS" name="gps" required>
           		   </div>
           		</div>
	  <div class="input-div pass">
		  <i class='bx bxs-comment-detail'></i> 
		<div class="div">
			<textarea type="text" placeholder="Descripción" id="description" name="descripcion" rows="5"></textarea>
	 </div>
  </div>
				<input type="hidden"  name="usuarioId" required value="{{$usuario -> id}}">
            	<input type="submit" class="btn" value="Registrar">
            </form>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('JS/car_main.js') }}"></script>
</body>
</html>

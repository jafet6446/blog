<!DOCTYPE html>
@extends('layouts.app')

@section('content')
<html lang="es">
  <head>
	  <title>Regístrate</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
      
    <script src="https://unpkg.com/ionicons@5.1.2/dist/ionicons.js"></script>
	
	  
	  <link href="{{ asset('css/lista.css') }}" rel="stylesheet">
	<style>
	button{
    width: 100px;
    height: 45px;
    margin: 0 10px;
    background: rgb(0,212,255);
    background: linear-gradient(90deg, rgba(0,212,255,1) 0%, rgba(2,101,255,1) 57%, rgba(0,212,255,1) 100%);
    border-radius: 30px;
    border: 0;
    outline: none;
    color: #fff;
    cursor: pointer;
	
    
		}
	
	</style> 
	  
   
  </head>
  <body style="background: #000428;
    background: -webkit-linear-gradient(to right, #004e92, #000428);
    background: linear-gradient(to right, #004e92, #000428);">

     
      <section class="Form my-4 mx-5">
                
				  <br>
              <h2 style="color:#FFFFFF"> {{ __('Mis vehiculos') }}</h2>
			  <br>
              <a href="{{ route('vehiculo.create') }}"><button style="width: 140px; height: 50px; background: linear-gradient(to right, #FF6669, #7B0002); ">Agregar Vehiculo</button></a>
				  <br></br>
				  @if($vehiculos)
				  <table class="table-bordered .table-responsive">
                      <thead>
                        <tr>
						  <th class="text-light">Numero de serie</th>
                          <th class="text-light">Marca</th>
                          <th class="text-light">Modelo</th>
                          <th class="text-light">Año</th>
                          <th class="text-light">Clase</th>
                          <th class="text-light">GPS</th>
                          <th class="text-light">Descripción</th>
						  <th class="text-light">Opciones</th>
							<th class="text-light">Eliminar</th>
                        </tr>
                      </thead>
					  <tbody>
				  	@foreach($vehiculos as $vehiculo)
				  		<tr>
							@if($vehiculo->usuarioId == $usuario->id)
                          <td>{{$vehiculo->numeroSerie}}</td>
                          <td>{{$vehiculo->marca}}</td>
                          <td>{{$vehiculo->modelo}}</td>
                          <td>{{$vehiculo->anio}}</td>
                          <td>{{$vehiculo->clase}}</td>
                          <td>{{$vehiculo->gps}}</td>
						  <td style="text-align: center"><textarea readonly id="description" name="description" rows="5">{{$vehiculo->descripcion}}</textarea></td>
                          <td style="text-align: center"><div><a href=""><button>Localizar</button></a>
							<a href="{{route('vehiculo.edit', ['id' => $vehiculo->id]) }}"><button>Actualizar</button></a>

							  </td>
							  <td style="text-align: center">
							<form method ="post" action="{{route('vehiculo.delete', ['id' => $vehiculo->id]) }}">
								@method('DELETE')
								@csrf
								<button style="background: linear-gradient(to right, #FF6669, #7B0002">Eliminar</button>
							  </form></div></td>
						  </tr>
							@endif
				  	@endforeach
						</tbody>
                    </table>
				  @else
				  	
				  		No se encuentran vehiculos registrados actualmente, por favor ingrese un vehiculo.
				 	 
				  @endif
				  
		     
                  
				 
          
          
          
       
      </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  @endsection
  </body>
</html>
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{ asset('CSS/Estilos_Form_prueba.css') }} " />
    <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
    <title>Formulario de Usuario</title>
  </head>
  <body>
    <div class="container">
      <div class="forms-container">
        <div class="signin-signup">
			
          <form method="POST" action="{{ route('login') }}">
			  @csrf
            <h2 class="title">Iniciar Sesión</h2>
            <div class="input-field">
              <i class='bx bx-mail-send'></i>
              <input id="email" type="email" placeholder="Correo Electronico" class="form-control my-1 p-2" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
				@error('email')
                    <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="input-field">
              <i class='bx bxs-lock-open-alt'></i>
             <input id="password" type="password" placeholder="Ingresa una contraseña" class="form-control my-1 p-2" name="password">
				 @error('password')
                     <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                     </span>
                 @enderror
            </div>
			 <button class="btn btn-primary font-weight-bold my-2 p-2">Iniciar sesión</button>
            
			  
			  @if (Route::has('password.request'))
                  <a href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                  </a>
			  @endif
          </form>

          <!--<form action="#" class="sign-up-form">
            <h2 class="title">Registrarse</h2>
            <div class="input-field">
            <i class='bx bxs-id-card'></i>
            <input type="text" name="Nombre" placeholder="Nombre">
          </div>
            <div class="input-field">
              <i class='bx bxs-user'></i>
              <input type="text" placeholder="Nombre de Usuario" />
            </div>
            <div class="input-field">
              <i class='bx bx-mail-send'></i>
              <input type="email" placeholder="Correo" />
            </div>
            <div class="input-field">
              <i class='bx bxs-lock-open-alt'></i>
              <input type="password" placeholder="Contraseña" />
            </div>
            <div class="input-field">
            <i class='bx bxs-lock-alt'></i>
            <input type="password" name="password" placeholder="Confirmar Contraseña">
          </div>
          <div class="input-field">
            <i class='bx bxs-phone'></i>
            <input type="phone" name="Telefono" placeholder="Telefono">
          </div>

            <input type="submit" class="btn" value="Registrar"/>
          </form>-->
        </div>
      </div>

      <div class="panels-container">
        <div class="panel left-panel">
          <div class="content">
            <h3>Nuevo Aqui?</h3>
            <p>
              ¡Si aún no tienes una cuenta registrate aquí!
            </p>
			 <form method="GET" action="{{ route('register') }}"> 
				<button class="btn transparent" id="sign-up-btn">
				  Registrarse
				</button>
			 </form>
          </div>
          <img src="img/Registro.svg" class="image" alt=""/>
        </div>
        <!--<div class="panel right-panel">
          <div class="content">
            <h3>Ya tienes una cuenta?</h3>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum
              laboriosam ad deleniti.
            </p>
            <button class="btn transparent" id="sign-in-btn">
              Iniciar Sesión
            </button>
          </div>
          <img src="img/InicioSesion.svg" class="image" alt=""/>
        </div>-->
      </div>
    </div>

    <!--<script src="JS/Formulario.js"></script>-->

  </body>
</html>

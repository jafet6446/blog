<!DOCTYPE html>
@extends('layouts.app')

@section('content')
<html lang="es">
  <head>
	  <title>Regístrate</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,300&display=swap" rel="stylesheet">  
      
    <script src="https://unpkg.com/ionicons@5.1.2/dist/ionicons.js"></script>
	
	  <link href="{{ asset('css/Estilos_Registro.css') }}" rel="stylesheet">
	  
    

  </head>
  <body>

    <section class="contact-box"> 
      <div class="row no-gutters bg-dark">
        <div class="col-xl-5 col-lg-12 img_registro">
          
          </div>
          <div class="col-xl-7 col-lg-12 d-flex">
              <div class="Contenedor aling-self-center p-6">
              <h2 class="font-weight-bold"> {{ __('Crea una cuenta gratis') }}</h2>
                  <div class="Contedor_formulario">
                      <button class="btn btn-outline-dark d-inline-block text-light mr-2 mb-3"><ion-icon class="lead align-middle mr-2" name="logo-google"></ion-icon>{{ __('Google') }}</button>
                      <button class="btn btn-outline-dark d-inline-block text-light mb-3"><ion-icon class="lead align-middle mr-2" name="logo-facebook"></ion-icon>{{ __('Facebook') }}</button>
                  </div>
                  <p class="text-muted mb-5"> {{ __('Ingrese la siguiente información para registrarse ') }}</p>
                  
				  
				  
                  <form method="POST" action="{{ route('register') }}">
					  @csrf
                      <div class="form-row mb-3">
                          
                          <div class="Contenedor_datos col-md-6">
                              <label for="name" class="font-weight-bold ">{{ __('Nombre') }}<span class="text-danger">*</span></label>
                              <input id="name" type="text" class="form_content form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Ingrese su nombre" required autocomplete="name" autofocus>
							  @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>
                          
                          <div class="Contenedor_datos col-md-6">
                              
                              <label for="username" class="font-weight-bold">{{ __('Nombre de usuario') }}<span class="text-danger">*</span></label>
                              <input id="username" type="text" class="form_content form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="Ingrese un nombre de usuario" required autocomplete="username" autofocus>
							  @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>
                      
                      </div>
                      <div class="Contenedor_datos mb-3 ">
                              
                              <label for="email" class="font-weight-bold">{{ __('Email') }}<span class="text-danger">*</span></label>
                          <br>
                              <input id="email" type="email" class="form_content2 form-control @error('email') is-invalid @enderror" name="email" placeholder="Ingrese un correo" value="{{ old('email') }}" required autocomplete="email">
						  @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                          
                          </div>
                      
                       <div class="Contenedor_datos mb-3">
                              
                              <label for="password" class="font-weight-bold">{{ __('Contraseña') }}<span class="text-danger">*</span></label>
                           <br>
                              <input id="password" type="password" class="form_content2 form-control @error('password') is-invalid @enderror" name="password" placeholder="Ingrese una contraseña" required autocomplete="new-password">
						   @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>
					  <div class="Contenedor_datos mb-3">
                              
                              <label for="password" class="font-weight-bold">{{ __('Confirmar contraseña') }}<span class="text-danger">*</span></label>
                           <br>
                              <input id="password_confirmation" type="password" class="form_content2 form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="Ingrese una contraseña" required autocomplete="new-password">
						   @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          </div>
                      
                      <div class="Contenedor_datos mb-3">
                              
                              <label for="phone" class="font-weight-bold">{{ __('Telefono') }}<span class="text-danger">*</span></label>
                           <br>
                              <input id="phone" type="phone" class="form_content2 form-control @error('phone') is-invalid @enderror" name="phone" placeholder="Ingrese un teléfono" value="{{ old('phone') }}" required autocomplete="phone">
                          </div>
                      
                      <div class="Contenedor_datos mb-5">
                      
                          <div class="form-check">
                          
                              <input class="form-check-input" type="checkbox">
                              <label class="form-check-label text-muted">{{ __('Al seleccionar está casilla, acepta nuestros términos y condiciones.') }}</label>
                              
                          </div>
                          
                      </div>
                      
                      <button class="btn btn-primary font-weight-bold">{{ __('Registrar') }}</button>
                  
                  </form>
              </div>
          
          </div>
          
        </div>
      </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  @endsection
  </body>
</html>
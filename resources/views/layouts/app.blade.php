<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'GPS Tracking System') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	
	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      
      <link rel="stylesheet" href="CSS/Home_Styles.css"> 
      
      <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
      
      <script src="https://kit.fontawesome.com/a02c47cedb.js" crossorigin="anonymous"></script>
	

	  <link href="{{ asset('css/Home_Styles.css') }}" rel="stylesheet">
	
</head>
<body>
    
		<nav class="Menu">
            <div class="logo">
                <h1><a href="{{url('home')}}">Gps Tracking System</a></h1>
            </div>
            
            <div class="container_elements">
                <ul class="elements">
					@guest
                    <li><a class="nav-link" href="{{url('home')}}" class="active">Inicio</a></li>
					<li><a class="nav-link" href="{{ route('login') }}">Iniciar sesión</a></li>
					@if (Route::has('register'))
                    <li><a class="nav-link" href="{{ route('register') }}">Registrarse</a></li>
                    @endif
					@else
					<li><a class="nav-link" href="{{url('home')}}" class="active">Inicio</a></li>
                    <li><a class="nav-link" href="{{ route('tablero.index') }}">Tablero</a></li>
					<li><a class="nav-link"href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> {{ __('Cerrar sesión') }}</a></li>
                                    

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                       @endguest
                </ul>
            </div>
            </nav>
		   

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
